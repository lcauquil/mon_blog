---
title: "Comment choisir une version de R spécifique au démarrage de RStudio en 1 clic"
author: L. Cauquil
date: "2024-09-12"
categories: [tips, RStudio]
format:
  html:
    toc: true
    toc-location: right
    toc-depth: 5
    toc-expand: true
from: markdown+emoji
---


RStudio nous donne la possibilité d'utiliser n'importe quelle version de R installée.

Pour cela il faut aller dans le menu:\
`Tools` → `Global Options...` onglet `General`\

![](select_r_version.png)

Puis redémarrer...

<br>

Pour lancer directement RStudio avec la version de R souhaitée, il suffit de tenir  la touche <kbd>Ctrl</kbd> appuyée et de cliquer sur le raccourci de RStudio <font size = 6>:wink:</font> 