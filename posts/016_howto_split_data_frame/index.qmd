---
title: "Comment couper un jeu de données en 2"
author: L. Cauquil
date: "2024-05-29"
categories: [tips, split, rsample]
format:
  html:
    toc: true
    toc-location: right
    toc-depth: 5
    toc-expand: true
---

Il s'agit d'une étape essentielle pour de nombreux modèles d'apprentissage automatique.

On divise les données en un dataset d'entraînement pour former le modèle et un dataset de test pour nous permettre de tester les prédictions du modèle. 

On utilise la fonction `initial_split()` du package `rsample`.\ Elle s'applique uniquement à des data frame.

## Package

```{r}
#| warning: false
#| message: false

library(rsample)
```

Exemple avec le jeu de données `iris`.\
Par défaut la fonction `initial_split` coupe le data frame suivant les proportions 3/4 - 1/4 (training - testing).

```{r}
split_iris <- initial_split(iris)
```

## Fonction `initial_split()`

L'objet créé est une liste, on a accès aux jeux de données coupés avec les fonctions `training()` et `testing()`.

```{r}
head(training(split_iris))
dim(training(split_iris))
```

```{r}
head(testing(split_iris))
dim(testing(split_iris))
```

<br>

L'argument `prop = ` permet de préciser la proportion utilisée pour scinder le data frame.\
Pour un partage en parts égales spécifier `prop = 0.5`.

```{r}
split_iris <- initial_split(iris, prop = 0.5)
dim(testing(split_iris))
dim(training(split_iris))
```

